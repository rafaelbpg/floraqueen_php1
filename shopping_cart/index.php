<?php
//Línea 263.

abstract class Product implements ProductInterface{
    public function getPrice() : float
    {
        return $this->price;
    }

    public function getName() : string
    {
        return $this->name;
    }
}

abstract class Voucher implements VoucherInterface{

    public function getDiscount() : float
    {
        return $this->discount;
    }
    
    public function getName() : string 
    {
        return $this->name;
    }
    
    public function getCondition() : string
    {
        return $this->condition;
    }
}

final class ProductA extends Product {
    public float $price = ProductsConstants::PRICES_LIST[ProductsConstants::PRODUCT_A];
    public string $name = ProductsConstants::PRODUCT_A;
}

final class ProductB extends Product {
    public float $price = ProductsConstants::PRICES_LIST[ProductsConstants::PRODUCT_B];
    public string $name = ProductsConstants::PRODUCT_B;
}

final class ProductC extends Product {
    public float $price = ProductsConstants::PRICES_LIST[ProductsConstants::PRODUCT_C];
    public string $name = ProductsConstants::PRODUCT_C;
}

final class VoucherR extends Voucher {
    public float $discount = VoucherConstants::DISCOUNTS[VoucherConstants::VOUCHER_R];
    public string $name = VoucherConstants::VOUCHER_R;

}

final class VoucherS extends Voucher {
    public float $discount = VoucherConstants::DISCOUNTS[VoucherConstants::VOUCHER_S];
    public string $name = VoucherConstants::VOUCHER_S;

}

final class VoucherV extends Voucher {
    public float $discount = VoucherConstants::DISCOUNTS[VoucherConstants::VOUCHER_V];
    public string $name = VoucherConstants::VOUCHER_V;
}

interface ProductInterface{

    public function getPrice() : float;
    
    public function getName() : string;
}
interface ShoppingCartInterface{

    public function addProduct(Product $product) : void;
    
    public function addVoucher(Voucher $voucher) : void;

    public function getId() : string;
    public function getTotal(): float;
}
interface VoucherInterface{

    public function getDiscount() : float;
    
    public function getName() : string;
    
    public function getCondition() : string;
}
final class ProductsConstants{
    
    const PRODUCT_A = 'PRODUCT A';
    const PRODUCT_B = 'PRODUCT B';
    const PRODUCT_C = 'PRODUCT C';

    public const CLASSES_LIST = [
        self::PRODUCT_A => ProductA::class,
        self::PRODUCT_B => ProductB::class,
        self::PRODUCT_C => ProductC::class,
    ];
    
    public const PRICES_LIST = [
        self::PRODUCT_A => 10,
        self::PRODUCT_B => 8,
        self::PRODUCT_C => 12
    ];
}
final class VoucherConstants
{
    const VOUCHER_S = 'VOUCHER S';
    const VOUCHER_V = 'VOUCHER V';
    const VOUCHER_R = 'VOUCHER R';
    
    const MIN_PRICE_TO_APPLY_VOUCHER_S = 40;

    public const CLASSES_LIST = [
        self::VOUCHER_S => VoucherS::class,
        self::VOUCHER_V => VoucherV::class,
        self::VOUCHER_R => VoucherR::class
    ];

    public const CONDITIONS_FOR_DISCOUNTS = [
        self::VOUCHER_S => '(5% discount on a cart value over 40€)',
        self::VOUCHER_V => '(10% off discount voucher for the second unit applying only to Product A)',
        self::VOUCHER_R => '(5€ off discount on product type B)',
    ];

    public const DISCOUNTS = [
        self::VOUCHER_S => 0.05,
        self::VOUCHER_V => 0.1,
        self::VOUCHER_R => 5
    ];
}
final class ShoppingCart implements ShoppingCartInterface {
    private string $id;
    
    private array $productsAdded = [];
    private array $vouchersAdded = [];
    private array $itemsAdded = [];
    private float $totalPrice = 0;
    private bool $isAppliedVoucherV = false;
    private int $quantityProductA = 0;
    private int $quantityProductB = 0;
    private int $quantityProductC = 0;
    
    public function __construct()
    {
        $this->id = uniqid("CartID-", true);
    }

    public function addProduct(Product $product): void
    {
        $this->productsAdded[] = $product;
        $this->itemsAdded[] = $product;
        $this->incrementeProductQuantity($product);
    }
    public function incrementeProductQuantity(Product $product) : void
    {
        if ($product instanceof ProductA){
            $this->quantityProductA++;
        }
        if ($product instanceof ProductB){
            $this->quantityProductB++;
        }
        if ($product instanceof ProductC){
            $this->quantityProductC++;
        }
    }
    public function addVoucher(Voucher $voucher): void
    {
        $this->vouchersAdded[] = $voucher;
        $this->itemsAdded[] = $voucher;
    }

    public function getTotal() : float
    {
        $this->resetTotalPrice();
        $this->resetAppliedVoucherV();
        $this->calculateTotalInProducts();

        $this->applicateVouchers();
        return $this->totalPrice;
    }

    public function resetTotalPrice(){
        $this->totalPrice = 0;
    }
    public function resetAppliedVoucherV(){
        $this->appliedVoucherV = false;
    }
    public function calculateTotalInProducts() : void
    {
        foreach($this->productsAdded as $product){
            $this->totalPrice += $product->getPrice();
        }

    }

    public function applicateVouchers(){
        
        $haveVoucherR = 0;
        $haveVoucherS = 0;
        foreach($this->vouchersAdded as $voucher){
            
            if ($voucher instanceof VoucherV and $this->canApplyVourcherV()){
                $this->applyVoucherV();
            }
            if ($voucher instanceof VoucherR){
                $haveVoucherR++;
            }

            if ($voucher instanceof VoucherS){
                $haveVoucherS++;
            }
        }
        $this->applyVourchersR($haveVoucherR);
        $this->applyVouchersS($haveVoucherS);


    }
    public function applyVourchersR($haveVoucherR){
        $canApplyMaxVoucherR = $this->quantityProductB;
        if ($haveVoucherR >= $canApplyMaxVoucherR){
            $this->totalPrice -= $canApplyMaxVoucherR * VoucherConstants::DISCOUNTS[VoucherConstants::VOUCHER_R];
        }
        else{
            $this->totalPrice -= $haveVoucherR * VoucherConstants::DISCOUNTS[VoucherConstants::VOUCHER_R];
        }
    }
    public function applyVouchersS($haveVoucherS){
        for($i = 0; $i < $haveVoucherS; $i++){
            if ($this->totalPrice < VoucherConstants::MIN_PRICE_TO_APPLY_VOUCHER_S){
                break;
            }
            $this->totalPrice -= VoucherConstants::DISCOUNTS[VoucherConstants::VOUCHER_S] * $this->totalPrice;
        }
    }
    public function canApplyVourcherV(){
        return $this->isAppliedVoucherV == false and $this->quantityProductA >= 2;
    }
    public function applyVoucherV(){
        $this->totalPrice = $this->totalPrice - VoucherConstants::DISCOUNTS[VoucherConstants::VOUCHER_V] * ProductsConstants::PRICES_LIST[ProductsConstants::PRODUCT_A];        
        $this->isAppliedVoucherV = true;
    }
    public function getId(): string
    {
        return $this->id;
    }
    public function printAllActions(){
        $actions = '';
        $i = 0;
        foreach($this->itemsAdded as $item){
            if ($i == 0){
                $actions .= $item->name . " ADDED ";
                $i = 1;
                continue;
            }
            if ($i > 0){
                $actions .= "+ " . $item->name . " ADDED ";
            }
            
        }
        echo $actions;
    }
}

$productA = new ProductA();
$productB = new ProductB();
$productC = new ProductC();

$voucherV = new VoucherV();
$voucherR = new VoucherR();
$voucherS = new VoucherS();
echo "\nExample 1:\n";

$car1 = new ShoppingCart();
$car1->addProduct($productA);
$car1->printAllActions();
$car1->addProduct($productC);
$car1->addVoucher($voucherS);
$car1->addProduct($productA);
$car1->addVoucher($voucherV);
$car1->addProduct($productB);
$total = $car1->getTotal();
$car1->printAllActions();
echo "\n> Total cart value: " . $total . "€\n";

echo "\nExample 2:\n";

$car2 = new ShoppingCart();
$car2->addProduct($productA);
$car2->addVoucher($voucherS);
$car2->addProduct($productA);
$car2->addVoucher($voucherV);

$car2->addProduct($productB);
$car2->addVoucher($voucherR);

$car2->addProduct($productC);
$car2->addProduct($productC);
$car2->addProduct($productC);

$total = $car2->getTotal();
$car2->printAllActions();
echo "\n> Total cart value: " . $total . "€\n";
