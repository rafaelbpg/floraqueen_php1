<?php
    /*Descripción:
    1. [5 - 10 minutes] Imagine you have a bag containing numbers: 1, 2, 3, ... 100. Each
    number appears exactly once, so there are 100 numbers. Now one number is
    randomly picked out of the bag. Find the missing number.
    Write a code implementation on a single PHP 7.x file that can be executed on the
    console.
    We expect to see printed the original bag, the bag without a random number and
    the missing number. Short and highly optimized code will be positively valued.
    */

    $bag = range(1, 100);
    echo "Original Bag";
    print_r($bag);
    unset($bag[random_int(1, 100) - 1]);
    echo "Bag with removed random element \n";
    print_r($bag);
    echo "The missing number is:\n ";
    echo current(array_diff( range(1, 100), $bag)); /* Lo hacemos de esta manera porque
    en principio el número es desconocido para nosotros (en el caso perfecto lo pudimos
    obtener de la línea quince y sumándole 1 a random_int(1, 100))
    */
    echo "\n";
    
  