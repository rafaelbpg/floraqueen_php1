En cada carpeta se encuentra cada problema, ejecutar los index.php desde
la consola y debería funcionar sin problemas.

En ambos problemas se pidió que estuvieran en un fichero único las soluciones.
Como veo que en el enunciado de los problemas, el tiempo era
importante (porque era una estimación que se hacía), missing_number fueron 2 minutos.
El Shopping Cart me llevó bastante más tiempo del estipulado en el enunciado; quizás atomicé
bastante más de lo evaluado. 

Está hecho con PHP7.x, por ende no hay algunas features nuevas de PHP8.0.